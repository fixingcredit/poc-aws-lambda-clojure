# poc-aws-lambda-clojure

This is a VERY basic proof of concept (POC) of using [Clojure](http://clojure.org/) with [AWS Lamdba](https://aws.amazon.com/lambda/).


## Getting Started

### Compile the code:

``` bash
lein uberjar
```

note: `STANDALONE` output jar, something like: `target/uberjar/poc-aws-lambda-clojure-0.1.0-SNAPSHOT-standalone.jar`

### Create Lambda Function

Using the AWS console, the steps roughly are:

1. Go to the [AWS Lambda console](https://us-west-2.console.aws.amazon.com/lambda/home)
1. Click "Create a Lambda function"
1. Skip selecting a blueprint
1. Create function
    1. Name your function, ie: `test-ping`
    1. Select runtime: Java 8
    1. Upload standalone JAR created above
    1. Specify handler function
        * Maps to Clojure namespace/class/function
        * `poc_aws_lambda_clojure.lambda_functions::ping`
            * [dashes in namespaces turn to underscores](http://stackoverflow.com/a/4451693)
            * separate package name from function/method via `::`
    1. Create an IAM Role for the Lambda function to operate under
        * Select "* Basic execution role" (NOTE: a popup should happen)
        * Name the role, something like "LambdaWithLogging"
        * This IAM Role gives the Lambda function permission to create/save logs using [CloudWatch Logs](http://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CWL_GettingStarted.html).
    1. Can leave memory and time at their default values
1. Confirm and click "Create function"
1. Click "Test" to validate the new function
    1. A modal will apear with the input to send to the function
    1. Enter `"User"` (with quotes...this will be deserialized to a String)
    1. Click "Save and Test"
1. See test results at the bottom of the screen (scroll...)
    * should see result `"pong - User"`

Success!


# TODO

* Show usage of the `com.amazonaws.services.lambda.runtime.Context` class
* Show usage of the AWS Lambda logging primitives